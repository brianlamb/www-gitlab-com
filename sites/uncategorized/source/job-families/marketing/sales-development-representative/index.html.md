---
layout: job_family_page
title: "Sales Development Representative"
---
 
<iframe width="560" height="315" src="https://www.youtube.com/embed/eHzmPIL-TB0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 
<br>
 
## Levels
 
### Associate SDR
 
The Associate SDR reports to the SDR Manager.
 
#### Associate SDR Job Grade
 
The Associate SDR is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Associate SDR Responsibilities
 
- Building target lists of account and leads based on lead flow and need within specific territories
- Create sequences based on Field Marketing requests
- Work leads from Field Marketing virtual events
- Support SDRs by researching accounts from targeted lists
- Support vacant territories as needed
- Build up to SDR activity metrics as leads become available
- Learn SDR Process and structure, specifically the SDR tech stack
- Prepare to move into SDR Role
- Complete Tanuki tech 100 level
 
#### Associate SDR Requirements  
 
- 6 plus months of work place experience
* Ability to demonstrate transferable skill set
- Willingness to be coached and trained
- Determination, curiosity, and creative problem solving
* If in EMEA, fluency in spoken and written German or French or other European languages will be an advantage
* If in LATAM, fluency in Portuguese and Spanish is required
* Willingness to work towards requirement and responsibilities of SDR (Intermediate)
* Ability to use GitLab
 
### SDR (Intermediate)
 
SDR (Intermediate) reports to the SDR Manager.
 
#### SDR (Intermediate)
 
The Associate SDR is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### SDR (Intermediate) Responsibilities
 
* Extends Associate the SDR responsibilities
* Effectively manage inbound lead flow as well as executing outbound prospecting initiatives
* Conduct high-level discovery conversations in target accounts
* Meet or exceed SDR sourced Sales Accepted Opportunity (SAO) volume targets
* Collaborate with and leverage teammates to develop targeted lists, call strategies, and messaging to drive opportunities
* Utilize business and industry knowledge to research accounts, identify key players, generate interest, create/identify compelling events, and develop accounts
* Work to have a variety of touches (call, email, social, etc.) on all leads in your assigned territory using Outreach.io
* Manage, track, and report on all activities and results using Salesforce
* Participate in documenting all processes in the GitLab handbook and update as needed with your Sales Development Manager
* Work in collaboration with Field and Corporate Marketing to drive attendance at regional marketing events
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Act as a mentor for new SDR hires in helping them navigate their key accounts
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts
* We’re a diverse team and we’re looking for you to bring your own unique flavor to the SDR role!
 
#### SDR (Intermediate) Requirements
 
* Extends the Associate SDR requirements
* Excited by the prospect of working cross-functionally with sales and different marketing departments. You'll have exposure to different departments like Sales, Marketing, Finance, Recruiting, Enablement, Engineering, etc. which will help you determine your career path at GitLab.
* Positive and energetic phone skills, excellent listening skills, strong writing skills
* A self-starter with a track record of successful, credible achievements
* You share our values, and work in accordance with those values
* Knowledge of business process, roles, and organizational structure
* Determined personality with a desire to grow and win
* Passionate about being a part of GitLab’s journey
* Proficient in using Salesforce and LinkedIn
* 2+ years work experience in a professional environment
* Previous tech industry experience or experience in sales development, marketing and/or sales is a plus
* Outbound prospecting experience is a plus
* Globally we require excellent written and spoken English which is our company language
 
### SDR (Intermediate - Level I)
 
* **Learning and Development:** Complete onboarding issues and pass SDR Technical Development Training Level I in your first 180 days
* **Performance:** The SDR must achieve 80% attainment for their total quota during months 1-3. 
 
* **Example**
    *   Month 1 quota 0
    *   Month 2 quota 6
    *   Month 3 quota 12
    *   Total onboarding quota = 18, 80% performance expectation = 14.4, rounded to 14. SDR must achieve a minimum attainment of 14 to remain in their role. 
 
### SDR (Intermediate - Level II)
 
* **Learning and Development:** Pass SDR Technical Development Training Level I and Training Level II
* **Performance:** Consecutively meet or exceed quotas for two quarters after achieving SDR Level I. This does not include ramped months.
 
### Senior SDR
 
The Senior SDR reports to the SDR Manager.
 
#### Senior SDR Grade Level
 
The Senior SDR is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Senior SDR Responsibilities
 
* Extends that of the SDR (Intermediate) responsibilities
 
#### Senior SDR Requirements
 
* Extends that of the SDR (Intermediate) requirements
* **Complete requirements for previous SDR levels**
* **Learning and Development:** Pass SDR Technical Development Training Level I, Training Level II  and Training Level III
* **Performance:** Consecutively meet or exceed quotas for two quarters after achieving SDR Level II. This does not include ramped months.
 
### SDR Team Lead
 
The SDR Team Lead reports to the SDR Manager.
 
#### SDR Team Lead Job Grade
 
The SDR Team Lead is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### SDR Team Lead Responsibilities
 
* Extends that of the SDR (Intermediate) responsibilities
 
#### SDR Team Lead Requirements
 
* Extends that of the SDR (Intermediate) requirements
* Complete requirements for previous SDR levels
 
* **Learning and Development:** Pass SDR Technical Development Training Level I and Training Level II Training Level III
* **Performance:** Consecutively meet or exceed quotas for two quarters after achieving SDR Level III. This does not include ramped months.
* The Team Lead role is based on business need and will be posted if/when there is a need.
 
## Specialties
 
### SDR (Public Sector)
 
#### SDR (Public Sector) Responsibilities
 
* Extends that of the Junior SDR, SDR (Intermediate) or Senior SDR responsibilities
* Focus on Public Sector customers
 
#### SDR (Public Sector) Requirements
 
* Extends that of the SDR Junior SDR, SDR (Intermediate) or Senior SDR requirements
* Previous experience working with Public Sector based customers or teams

### SDR (Land)

SDR (Land) focuses on acquiring new logos.
 
#### SDR (Land) Responsibilities

* Extends that of the Junior SDR, SDR (Intermediate) or Senior SDR responsibilities
* Leading account planning efforts and creating in-depth account plans for top focus accounts
* Qualifying and converting marketing-generated leads (MQLs) and inquiries into sales accepted opportunities (SAOs)
* Aligning prospecting efforts to the field, corporate, strategic and account based marketing campaigns
* Educating prospects and making positive impressions
* Generating awareness, meetings, and pipeline for sales
* Collaborating with peers, marketing, and sales teams
  
#### SDR (Land) Requirements

* Extends that of the SDR Junior SDR, SDR (Intermediate) or Senior SDR requirements
* Meet monthly quota
* Maintain a high sense of autonomy to focus on what's most important
* Participate and lead planning, execution, and cross-functional meetings
* Participate in initial qualifying meetings, discovery calls, and follow-up conversations
* Maintain exceptional Salesforce hygiene, logging all prospecting activity, conversations, and account intelligence uncovered
* Generate IACV Pipeline
  
### SDR (Expand)

SDR (Expand) focuses on existing customers and their subsidiaries.
 
#### SDR (Expand) Responsibilities

* Extends that of the Junior SDR, SDR (Intermediate) or Senior SDR responsibilities
* creating qualified meetings and sales accepted opportunities with net new customers in the Large - Enterprise Segment
* focus on account-centric, persona-based outreach, strategic outbound prospecting, social selling and creative outreach to educate and create internal champions that align with the GitLab value drivers and use cases.
 
#### SDR (Expand) Requirements

* Extends that of the SDR Junior SDR, SDR (Intermediate) or Senior SDR requirements
 
## Performance Indicators
 
* Sales Accepted Opportunity Attainment vs Goal
* Net ARR Pipeline Generated
* Initial Qualification Meeting (where applicable)
 
## Career Ladder
 
Subject to business need, GitLab team members from the Sales Development Representative job family typically interview for roles such as: [Manager, Sales Development](https://about.gitlab.com/job-families/marketing/sales-development-manager/), [Inside Sales Representative](https://about.gitlab.com/job-families/sales/public-sector-inside-account-representative/), and [Account Executive, SMB](https://about.gitlab.com/handbook/sales/commercial/smb/). All of these roles require a formal application and interview process, and are not considered a given promotion.
 
### SDR Promotion Process 
 
When an SDR qualifies for a promotion, they need to make a copy of the [SDR Promotion Template](https://drive.google.com/drive/search?q=%22SDR%20Promotion%20Doc%20Template%22), complete the doc, and share with their Manager. The SDR Manager will confirm that all areas are complete before submitting the promotion in Bamboo. 
 
**Required fields in the SDR Promotion Template:
* Quarterly attainment details
* List of [Tanuki Tech](/handbook/marketing/revenue-marketing/sdr/tanuki-tech/) courses required for promotion level
* Examples of the SDR living [GitLab Values](/handbook/values#credit)
 
### SDR Performance Expectations
 
After one quarter of performing at a combined attainment of 80% or less of goal the SDR will receive a written warning of their performance. If in month 4 the SDR does not meet or exceed 80% of their combined goal they will go on a 30-day performance management plan. Performance management interventions are implemented by the SDR’s direct manager. Attainment is not the only reason for a performance improvement plan, additional information about GitLab code of conduct can be found [here](/handbook/people-group/code-of-conduct/) and how GitLab manages underperformance is detailed [here](/handbook/leadership/underperformance/).
 
## Hiring Process
 
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).
 
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the SDR Manager for the region they have applied for
* Candidates will then be invited to schedule an interview with a regional leader
* Following successful interviews, candidates will then be invited to complete a final writing assessment
 
Additional details about our process can be found on our [hiring page](/handbook/hiring/).
