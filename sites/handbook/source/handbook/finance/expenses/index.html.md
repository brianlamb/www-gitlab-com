---
layout: handbook-page-toc
title: Expenses
---

## On this page
{:.no_toc}

- TOC
{:toc}

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Introduction

Welcome to the Expenses page!  You should be able to find answers to most of your questions here.  If you can't find what you are looking for, then:
- **Chat channel**: `#expense-reporting-inquires`
- **Email**: `ap@gitlab.com`

GitLab utitilizes Expensify as our team member expense reimbursement tool. All team members will have access to Expensify within 2 days from their hire date.  If you didn't receive an email from Expensify for your access, please contact ap@gitlab.com. Note: Ensure to opt-out from news by logging in and navigating to `Settings > Preferences > Contact Preferences` and un-checking the box at `Relevant feature updates and Expensify news`.

Expense reports are to be submitted at least once a month. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here](https://community.expensify.com/discussion/5922/deep-dive-day-1-with-expensify-for-submitters/p1?new=1).

### <i class="far fa-paper-plane" id="biz-tech-icons"></i> Redacting Personal Information from Receipts - A Caution
As the service does not require your personal information in order to process the reimbursement, we encourage you to redact your personal information before uploading receipts to protect your privacy. Managers should relay this caution to your team members, but we will no longer deny the expense report if receipt images contain personal information.

Consider redacting this information:

- Your shipping and billing address(es)
- Your credit card information
- Your full name
- Any other data that is personal to you that should not want publicly known

At a minimum, information about the items/products purchased and the total purchase amount should be visible so managers can validate the expense reimbursement amount requested along with the actual items and total purchase price.

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and [subsidiary assignment](https://about.gitlab.com/handbook/tax/#tax-procedure-for-maintenance-of-gitlabs-corporate-structure). Check with Payroll if you are unsure about either of these.

For general information and guidelines regarding the company expense policy, check out the section of our team handbook on [Spending Company Money](/handbook/spending-company-money). Managers and the Accounts Payable team will review expenses for compliance with the company travel policy.  The CEO will review selected escalations at least annually.

Team members should also consider the terms and conditions of their respective contractor agreements when submitting invoices to the company.

Team members in a US policy will be automatically reimbursed through Expensify after their report is "final approved" within 7 business days by Accounts Payable team. For all other team members, please see the reimbursement process below based on your location or employment status.

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> Office Equipment and Supplies

The company will reimburse for these [office equipment and supplies](/handbook/finance/procurement/office-equipment-supplies/) if they **assist you in achieving greater business [results](/handbook/values/#results)**, and local law allows us to pay for items without incurring payroll taxes. Please keep in mind that while the amounts below are guidelines and not strict limits, any purchase (other than a laptop) that will cost GitLab $1000 USD per item (or over) will require prior approval from your Manager and Accounting.

When you evaluate the value of greater results relative to the cost of an item, we encourage you to [spend company money like its your own](/handbook/values/#spend-company-money-like-its-your-own). This means weighing the cost of a dollar spent to the value that it brings to the business, just as you would assess a personal dollar that you spend against the relative benefit that it brings you.

When reimbursing through Expensify please select "reimbursable" for the portion which GitLab will be covering, and add a note explaining the receipt difference.

* Please note Expensify cannot sync "non-reimbursable" line items, only reimbursable line items are to be entered in Expensify.

Team members should not use a Corporate Credit Card to purchase office equipment for their personal workspace.  All office equipment purchases for a team member's personal workspace should be made on a personal credit card and expensed.

For Laptop Purchases/Refreshes, please refer to [IT Ops Laptop](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptops) policy and procedure.

### Obtaining pre-approval from Accounting 

In order to prevent issues during the reimbursement of purchases that go above the guideline price listed in our [office equipment and supplies](/handbook/finance/procurement/office-equipment-supplies/) handbook page, we recommend getting pre-approval before making a purchase and submitting for a reimbursement. This isn't required for purchases below the guideline price. 

Keep in mind that GitLab won't reimburse items that are included in the [list of non-reimbursable expenses.](/handbook/finance/expenses/#-non-reimbursable-expenses) 

To obtain pre-approval, post in the #[expense-reporting-inquires](https://gitlab.slack.com/archives/C012ALM8P29) slack channel - the equipment you are purchasing, the cost of the item and the reason why the purchase is above the guideline price (this could be due to higher taxes in your region for example). Tag your manager as well so they are aware of the ask and they can help provide context if needed. Someone from our Accounts Payable team will reply to you with approval. When you are submitting your expense, add a note saying "`Jane Doe (name of approver)` pre-approved this expense".

### Rejected expenses

Sometimes expenses get rejected by our third party accounting team. Before following the steps below, please ensure your purchases don't fall in our [list of non-reimbursable expenses.](/handbook/finance/expenses/#-non-reimbursable-expenses). An expense might get rejected if it goes above the guideline price listed in our [office equipment and supplies](/handbook/finance/procurement/office-equipment-supplies/) handbook page. 

**If you didn't get pre-approval from the accounting team before submitting the expense,** then start a new post in the #[expense-reporting-inquires](https://gitlab.slack.com/archives/C012ALM8P29) slack channel and explain the situation. Follow the same format as described in [requesting pre-approval](#obtaining-pre-approval-from-accounting). Once you get approval, submit your expense again, highlighting that the accounting team has given their thumbs-up to the expense. 

**If you got pre-approval from the accounting team before submitting the expense,** then go back to your original post and follow up with the person who approved the item and share the rejection message with them so the team can follow up with our third party accounting team.

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Setting up a home office for the first time?

Take inspiration from our [all-remote page covering key considerations for a comfortable, ergonomic workspace](/company/culture/all-remote/workspace/). You can also consult the `#questions` and `#remote` channels in Slack for recommendations from other GitLab team members.

### <i class="fas fa-bullseye" id="biz-tech-icons"></i>  Software


1. We do not issue Microsoft Office 365 licenses, as GitLab uses Google Workspace
   ([Docs](/handbook/communication/#google-docs), Slides, Sheets, etc.) instead.
1. For security related software, refer to the security page for [laptop and desktop
   configuration](/handbook/security/#laptop-or-desktop-system-configuration).


#### Individual Subscriptions

GitLab does not reimburse individual software subscriptions (e.g. Apple Music, Krisp, etc.). Software subscriptions should be set up through the [company procurement process](/handbook/finance/procurement/), or by [creating an access request for an existing company subscription](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/).

##### Subscription Exceptions

Certain types of individual subscriptions are reimbursable, such as VPN service. If you are uncertain whether a particular service is reimbursable, please contact Finance in the `#expense-reporting-inquiries` channel.

If you need a short-term or individual software subscription, or a single-pay software license, please reach out to Finance in the `#accountspayable` Slack channel to discuss options before acquiring the software.                                                                                                    |

### Other
1. Business cards ordered from Moo as per the [instructions](https://about.gitlab.com/handbook/people-group/frequent-requests/#business-cards) provided by the People Experience team.
	*Urgent Business cards needed for day of start can be requested by emailing people-exp@gitlab.com. As a last resort, Moo does offer 3 to 4 Day Express service.*
1. Work-related books

### Transport/Delivery of free procurements
Feel free to check local second-hand/free markets when looking for equipment, especially furniture such as desks and chairs. GitLab will reimburse the cost of any transport and delivery services you need to procure the item(s) provided the total cost is reasonable based on the table above factoring in any local pricing adjustments.

GitLab will also reimburse costs relevant to one's location in cases where anniversary gifts or company issued swag incurs additional import costs. 

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Reimbursements

The company will reimburse for the following expenses if you need it for work or use it mainly for business, and local law allows us to pay for it without incurring taxes:
1. Mileage is reimbursed according to local law: [US rate per mile](http://www.irs.gov/Tax-Professionals/Standard-Mileage-Rates), [rate per km in the Netherlands](http://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/zakelijk/auto_en_vervoer/auto_van_de_onderneming/autokosten/u_rijdt_in_uw_eigen_auto), or [rate in Belgium](https://fedweb.belgium.be/nl/verloning_en_voordelen/vergoedingen/vergoeding-voor-reiskosten). Add a screenshot of a map to the expense in Expensify indicating the mileage.
1. Internet connection subscription.
	* For team members outside the Netherlands: follow normal expense report process.
	* For team members in the Netherlands: fill in and sign the [Regeling Internet Thuis](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) form and send it to the People Experience team at `people-exp@ gitlab.com`. The People Experience team will then send it to the payroll provider in the Netherlands via email. The details of the payroll provider can be found in the PeopleOps vault in 1Password under "Payroll Contacts".
  * This is a taxable expense for GitLab Ltd. team members assuming that the internet connection is used partially for personal use as well as business use.
1. VPN service subscription. Please read [Why We Don't Have A Corporate VPN](/handbook/security/#why-we-dont-have-a-corporate-vpn), and check out our [Personal VPN page](/handbook/tools-and-tips/personal-vpn/) regarding usage at GitLab.
1. Mobile subscription, we commonly pay for that if you call a lot as a salesperson or executive, or if your position requires participation in an on-call rotation. For on-call, you may expense the cost of your mobile phone service for the month when you begin your on-call rotation. This is limited to your service cost itself, not any costs relating to the phone device, to a personal hotspot device or to services for other people on your phone plan. You may include additional data charges in your expense report as we understand you may have plans outside of your normal workspace while you're on-call. 
1. Telephone land line (uncommon, except for positions that require a lot of phone calls)
1. Skype/Google Hangouts calling credit (uncommon, since we mostly use [internet-based services such as Zoom](/blog/2019/08/05/tips-for-mastering-video-calls/))
1. Laptops, insurance and repairs
    1. The [IT Ops](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptops) page outlines laptop purchasing for new hires and for repairs and EOL for existing employees.
    1. Laptops paid for by the company are property of GitLab and need to be reported with serial numbers, make, model, screen size and processor to IT Ops by adding it to this form: [GitLab laptop information](https://docs.google.com/forms/d/e/1FAIpQLSeUOlP11qeLdLZHTI62CFr7MSHAoI_1M6CRpnUA6fegkEKCOQ/viewform) for proper [asset tracking](/handbook/finance/accounting/#fixed-asset-register-and-asset-tracking). Since these items are company property, you do not need to buy insurance or an extended warranty for them unless it is company policy to do so (for example, at the moment we do not purchase Apple Care or extended warranties). You do need to report any loss following [Lost or Stolen Procedures](/handbook/people-group/acceptable-use-policy/#lost-or-stolen-procedures) or damage to IT Ops as soon as it occurs.
    1.  **Repairs to company issued equipment.**
        * If you need to replace a battery or something small that does not affect the productivity or speed of the device, please go ahead and get that small item replaced and expensed.
        * Please get approval from your Manager if your equipment appears to be damaged, defective, or in need of repair. Business Operations can advise on next steps to ensure you have the proper equipment to work.
        * For loaner laptops: Do not hesitate when expensing a loaner laptop while your primary laptop is being repaired. Use your best judgment identifying a local vendor. Please check out our [laptop repair](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptop-repair) page for more info.
1. English lessons. At GitLab the lingua franca is [US English](/handbook/communication/#american-english), when English is not your native language it can limit you in expressing yourself.


### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Coworking or external office  space
If working from home is not practical you may submit for reimbursement for the cost of a co-working space. This can include non-traditional spaces that require a recurring (full-time monthly) membership as long as you average at least ~4 working days per month at the space. If flexible membership options exist in the form of daily passes or hourly packages, then these can be expensed as well, as long as the prorated cost per month does not exceed that of a recurring membership subscription. For instance, if both the monthly subscription and a hypothetical 10-day pass is $200 USD, then you can only expense one such pass each month.

Any agreement must be between the team member and the co-working space (i.e. GitLab will not sign or appear on the agreement). All expenses must be submitted through the normal [travel and expense reimbursement policy](/handbook/finance/accounting/#reimbursable-expenses). The Company will not be responsible for the deposit, and therefore will not reimburse for the upfront deposit. This will solely be the responsibility of the employee. In addition, the company will not be responsible for any expense that relates to office space subsequent to the termination of service between GitLab and the team member.


### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Work-related online courses and professional development certifications

GitLab team members are allotted [$10,000 USD](https://www1.oanda.com/currency/converter/) per calendar year to spend on one or multiple training courses. Follow the process for submitting a [Growth and Development Reimbursement](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit). Expenses related to reimbursement of tuition will be capped at $1,000 USD within Expensify. Anything over $1,000 USD must go through payroll for reimbursement. Team members must attach a screenshot of the G&D issue when submitting a expense report for tuition, while choosing the "Training" category option. Please note any sort of conference attendance should be categorized to "Conference Attendance", and follow the same G&D steps as outlined above with reimbursement in Expensify capped at $1,000 USD.

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Team Manager Flower and Gift Purchases ###

Managers are able to send Gifts and Flowers on behalf of their team members in acknowledgment of significant life events such as the birth of a little one; well wishes ahead of surgery, or the loss of a loved one.

The event in question must pertain to a GitLab Team Member or the immediate family of a GitLab Team Member and will be allocated to the respective team members departmental budget - the spend range for significant life events is **$75 to $125**. 

Managers can facilitate the ordering of Flowers or Gifts for delivery, but please note that you are unable to send restuarant gift cards at this time.  In an instance where you would like to extend the offer of a meal or food delivery service, this will need to be expensed by the recipient for reimbursement.

When expensing Team member gifts please use the tag FY22_EmployeeGiftsFlowers.

### Team building Budget

In FY22, each eGroup member has been allocated $50 per team member per quarter for FY22-Q1 to FY22-Q3 for team building events. There is an additional budget for $100 per team member in FY22-Q4. More to come in the section below as we get closer to FY22-Q4.

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Year-end Holiday Party Budget

In previous years, GitLab has allocated [$100 USD](https://www1.oanda.com/currency/converter/) per GitLab team member for a holiday event in December. Due to continued travel restrictions, each eGroup member will be responsible for setting the holiday event guidelines for their teams.  The budget per team member is $100 USD. 

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> Travel and Expense Guidelines

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Travel

1. If you are taller than 1.95m or 6'5", you can upgrade to Economy Plus. There is no dollar restriction on this since it will be hard to fit in economy with that height.
1. For flights longer than 8 hours, you can expense:
   * Up to the first [$300 USD](https://www1.oanda.com/currency/converter/) for an upgrade to Business Class on flights longer than 8 hours if you are taller than 1.95m or 6'5".
    * Up to the first [$100 USD](https://www1.oanda.com/currency/converter/) for an upgrade to Economy Plus (no height restriction) on flights longer than 8 hours.
1. GitLab does not cover expenses for significant others or family members for travel or immigration. This includes travel and visas for GitLab events.
	* There are other things that [the company will not reimburse](/handbook/finance/accounting/#8-employee-reimbursements---expensify), such as dog boarding.

For additional Company Travel related questions, please refer to our [Travel Handbook](/handbook/travel/#booking-travel-through-tripactions-) regarding booking travel through TripActions.

### Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and Contributes.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-–-soda’s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Renting Cars

#### US and Canada

##### Third Party Liability

Purchase the liability insurance that is excess of the standard inclusion of State minimum coverage in the rental agreement at the rental agency. GitLab’s insurance policy provides liability insurance for rental cars while conducting company business, but it may be excess over any underlying liability coverage through the driver or credit card company used to purchase the rental.

Purchase the liability offered at the rental counter if there are foreign employees renting autos in the US or Canada. While workers' compensation would protect an injured US employee, other passengers may have the right to sue. To ensure that GitLab has protection when a foreign employee invites another person into the car we recommend the purchase of this insurance when offered at the rental counter.

##### Physical Damage - Collision Damage Waiver

**Do Not** purchase the Collision Damage Waiver offered at the rental counter. GitLab purchases coverage for damage to rented vehicles.
 If travel to Mexico is required, **purchase** the liability insurance for Mexico offered at the rental counter. You should verify that the rental agreement clearly states that the vehicle may be driven into Mexico and liability coverage will apply.

#### Countries other than the US and Canada

##### Third Party Liability

Purchase the liability insurance offered at the rental counter when traveling outside the US and Canada. Automobile Bodily Injury and Property Damage Liability insurance are required by law in almost every country. Please verify this coverage is included with the rental agreement.

##### Physical Damage - Collision Damage Waiver

Purchase the Collision Damage Waiver or Physical Damage Coverage offered by the rental agency when traveling outside the US and Canada.

In the event of an accident resulting in damage to the rental car, the foreign rental agency will charge the credit card used to make the reservation with an estimated amount of repair costs if insurance is not purchased. If this happens, GitLab does not purchase Foreign Corporate Hired Auto Physical Damage Coverage to reimburse for damages.

### Virtual Meal with GitLab Team member(s)

Currently Suspended

### Something else?

No problem, and consider adding it to this list if others can benefit as well.

1. Customer/Partner Facing Events
    * Gala/Black Tie Events: Tuxedo or Gown Rental, $150-$225 USD per event.
    * The event must be customer specific and the invitation must state black tie only.

## <i class="far fa-flag" id="biz-tech-icons"></i> Expense Reimbursement

1. Effective 2019-07-01, all expense reports must be submitted to your manager for approval prior to being sent to Montpac, and Accounts Payable for approval and reimbursement.
1. If you are a team member from Nigeria, please submit your expense in your salary invoice (a template can be found [here](/handbook/finance/#invoice-template-and-where-to-send)) with receipts attached to <nonuspayroll@gitlab.com>.  Please note, this is a temporary solution while we are transition over to a PEO.
1. If you are a team member and incurred an expense charged in a currency different from the one you use to submit your invoices, use the conversion rates specified in the [global compensation section of the handbook](/handbook/total-rewards/compensation/#exchange-rates). If the expense currency doesn’t exist in that list, refer to the conversion rates in [oanda](https://www.oanda.com/currency/converter/). Make sure to set the expense date in the currency converter form.
1. GitLab uses Expensify to facilitate the reimbursement of your expenses. As part of onboarding you will receive an invitation by email to join GitLab's account. Please set up your account by following the instructions in the invitation.
	* If you are a team member in Spain or France, please submit your expenses through Safeguard in-house expense reimbursement management system and also submit them through Expensify.  Accounts Payable will review, approve, and send the approval of your expense reports in Expensify to your gitlab email address.  You will need to forward the approval email to Safeguard enable for them to process your expense reimbursement via payroll.

	* If you are new to Expensify and would like a brief review, please see [Getting Started](https://community.expensify.com/discussion/7703/getting-started-video)
	* For step by step instructions on creating, submitting, and closing a report please see [Create, Submit, Close](https://docs.expensify.com/en/articles/2921-report-actions-create-submit-and-close)
	* For US team members, the approved expense amount will be deposited into your account a few days after the report has been approved by Accounts Payable.
	* For Australia, New Zealand, Belgium, Germany, Netherlands, Ireland, and Japan AP will process the approved report on Friday.  The payment will be deposited into your account no later than three business days the following week. 
    Accounts payable uses Tipalti to drive payment to the above policies. As part of onboarding, an invitation to the Tipalti portal will be sent. Please sign up and onboard only banking information, no tax information is needed. If set up of the personal Tipalti account is not completed in a timely manner, this may result in a delay of expense payment.

	* For all team members being paid by Safeguard, Remote, Global Upside, CXC, iiPay, or Vistra, the approved expense amount will be deposited in your account with your monthly salary.

1. If you are a team member with a company credit card, your company credit card charges will automatically be fed to a new Expensify report each month. Please attach receipts for all expenses (per the Expense Policy, see below) within 1 business days after the end of the month. These amounts will not be reimbursed to you but Expensify provides a platform for documenting your charges correctly.

### Reimbursement process and timeline:

##### SafeGuard

The list of SafeGuard countries can be found [here](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity)

Team members who are employed through SafeGuard must submit their expense for reimbursement through Expensify.  All expense reports must be submitted and approved by the manager and the Accounts Payable team by the 8th of the month to be included in the current month payment. **All Expense Reports must be approved by the Manager, and Montpac by EOD on the 7th of the month**

Team members working via SafeGuard must submit their expenses through:
*  Expensify
*  Safeguard in-house expense reimbursement
*  GitLab payroll send the expense approval to Safeguard after the team member's manager approved the report
*  Team members send the original receipts to Safeguard

##### Global Upside & Remote.com

The list of Global Upside & Remote countries can be found [here](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity)

* Team members must submit their expenses through Expensify. All expense reports must be submitted and approved by manager and the Accounts Payable team by the 7th of the month to include in the current month payment. 

##### iiPay

* All Individual contractors or C2C, with exception of Nigeria will be reimbursed by iiPay by the 22nd of each month.  All expense reports must be approved by the manager and the Accounts Payable team by the 8th of each month to be include in the current month payment.  For contractor with C2C status, be sure to contact Payroll team via email at nonuspayroll@gitlab.com and ap@gitlab.com if you need to set up a separate bank for your expense reimbursement. 

##### Legal entities

* Expense reports for GitLab Ltd (UK) must be approved by the manager and the Accounts Payable team on or before the 14th of each month in order for the reimbursement to be include in the current month payroll.
* Expense reports for GitLab Canada Corp must be approved by the manager and the Accounts Payable team before the 1st day of each payroll period.  Please see [Payroll Calendar](https://docs.google.com/spreadsheets/d/1ECkI_Z8R82j1eipJEEybXjO-EDtzw4TuhJPOnHypDho/edit#gid=0) for the payroll cut off date.
* Expense reports for GitLab France S.A.S must be approved by the Manager and the Accounts Payable team on or before the 08th of each month in order for the reimbursement to be included in the current months Payroll.
* Expenses reports for GitLab Korea Limited must be approved by the Manager and the Accounts Payable team on or before the 08th of each month in order for the reimbursement to be included in the current months Payroll.
* Expense reports for Singapore PTE. LTD. must be approved by the Manager and the Accounts Payable team on or before the 08th of each month in order for the reimbursement to be included in the current months Payroll.
* Expense reports for GitLab BV (Belgium and Netherlands), GitLab GmbH (Germany), GitLab PTY Ltd (Australia and New Zealand), GitLab GK (Japan), and GitLab LTD (Ireland) are reimbursed via GitLab AP within 10 business days from the approval date by their manager and the Accounts Payable team.
* Expense reports for GitLab Inc, GitLab Inc Billable, and GitLab Federal reimbursed via Expensify, and AP will final approve the report within 5 business days after the approval from their manager.

##### Nigeria

* Please include your expenses along with receipts on your monthly salary invoice.

##### CXC Global 

The list of CXC countries can be found [here](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity)

* Expense reports must be submitted in Expensify by team members, approved by their managers, and final approved by Accounts Payable team on or before the 7th of each month to ensure it is included in the current months payroll
* GitLab Payroll will send the approved expense amount to CXC EMEA payroll to include with the monthly salary
* Team members must include the approved expense amount on their monthly invoice as well.


## <i class="far fa-flag" id="biz-tech-icons"></i> Expense Policy

1. Max Expense Amount - [$5,000 USD](https://www1.oanda.com/currency/converter/)  - NOTE - If you are a corporate credit card holder, please refer to the [corporate credit card policy section](/handbook/finance/accounting/#credit-card-use-policy) for those specific instructions and thresholds.
1. Itemized receipts are required for all expenses.
1. Expenses should be submitted within 30 days of purchase, spend, or invoice due date. This helps the Company to better manage our budget and ensures accurate month-end reporting.
1. Expense report items should clearly state if the spend includes amounts for clients / non-team members.  Tax requirements in certain countries require us to account for client spend differently.
1. All team members must submit their expense reports in their designated policies in Expensify - COGS team members in COGS policies and non-COGS team members in non-COGS policies.
1. Giftcards are not accepted as a form of payment for business expenses.
1. All internet/telephone reimbursements must have a detailed receipt attached which shows the amount, service, and service date.

* Please note we do not reimburse late fees, intial set up, or equipment costs.

## Team Member Expense Temporary Advances

These instructions apply if a team member is unable to purchase required items, for whatever reason.

1. The team member will make a list of requested items and prices, noting if they are out of the budget range listed in the [Expenses handbook section](/handbook/finance/expenses/) (if applicable), and send it to their manager for approval. We ask that only one list be sent, versus multiple lists.
1. The team member's manager will send the approved (or edited) list to Accounting (nonuspayroll@gitlab.com OR uspayroll@gitlab.com, and CC ap@gitlab.com) for final approval and dispensation.
1. Once approved, Payroll will send the team member an invoice template to fill out with the approved items, prices and the team member's bank information.
1. The approved final amount will be sent to the team member's bank and they can then purchase their approved items.
1. Receipts should be submitted to ap@gitlab.com after the purchases have been made.

## <i class="far fa-flag" id="biz-tech-icons"></i> Approving Expense Reports

1. All expense reports are approved by the team members direct manager or their designated approver when they are out of the office.
1. It is expected that the expense report approver will perform a complete review to ensure the reasonableness and accuracy of the submitted expenses.
1. Expensify will send a notification email to the designated approver when a team member submits an expense report.
    * Click on the report name in the body of the email
    * Review each expense for the correct amount of the receipt and the report
    * Check for customers or project name if applicable under Tag
    * We required a receipt for any expense greater than a $5 cash purchase (except for Billable policy)
    * Select [Approve and Forward] option and Expensify pre-populated the email address.  Note, Expensify is updating their coding to address a small glitch in this field.  If it is empty, please send it to **Montpac** (gitlab-expensify-mp@montpac.com)
    * **Important** - please do not use [Final Approval] because Expensify will not send the email notification for payment approval and it will delay the reimbursement process
    * Manager can delegate the approval process during PTO:
        *  Settings
        *  Your Account
        *  Vacation Delegate
        *  Enter the email address of the backup approval
    * All expense question(s) can be addressed via expenses@gitlab.com or in the #Finance and #expense-reporting-inquires
Slack channel

1. <i class="fas fa-bullseye" id="biz-tech-icons"></i>  **Expenses Reports approval deadline**
    * Australia, Germany, New Zealand, Netherlands, United States, New Zealand, Ireland, Japan - After approval completion by manager and Accounts Payable.
    * United Kingdom - All expense reports must be approved by the manager and Accounts Payable no later than the 14th of each month.  Team members - please be sure to submit your report(s) a couple days before the due date so your manager and Accounts Payable have enough time for approval.
    * Canada - All expense reports must be approved by manager and Accounts Payable before 1st day of each payroll period.
    * All non-US contractors - All expense reports must be approved by manager and Accounts Payable no later than the 8th of each month. Team members - please be sure to submit your report(s) a couple days before the due date.

## <i class="far fa-flag" id="biz-tech-icons"></i> Non-reimbursable Expenses

As we ask team members to [spend company money like its your own](/handbook/values/#spend-company-money-like-its-your-own), we try not to micromanage spending. Our spending sub-value is the foundation of our expense philosophy, and we've found that the vast majority of team members exercise thoughtful judgement in purchasing. That said, we've had some questionable purchases. Some examples of purchases that have felt incongruent with the spirit of our spending sub-value include:

1. Costume for the party at the end of Contribute
1. Boarding expense for animals while traveling
1. A $1700 office chair from a celebrity furniture designer 
1. A llama rental for a team event
1. Meals during the Contribute when team members opt out of the company provided meal option
1. Cellphones and accessories
1. Travel-related expenses for family members of GitLab employees
1. Fitness equipment (treadmill, etc..) and gym membership
1. Meals from co-working day(s)
1. Home office decor (picture frames, hanging plants, area rugs, book shelves)
1. Meals or beverages while attending co-working space
1. Treadmill Desks
1. Portable Air Conditioners
1. Home Power Generators 
1. [Individual subscriptions ex. Krisp, Apple Music](#individual-subscriptions)

You should proactively engage your manager if you plan to make a work purchase that is outside of what would be considered standard by an average team member. Please [say why, not just what](/handbook/values/#say-why-not-just-what) when you are explaining what you are purchasing and why.

## <i class="far fa-flag" id="biz-tech-icons"></i> Independent Contractors

In accordance with [Reimbursable Expense guidelines](/handbook/finance/accounting/#reimbursable-expenses), independent contractors should note which expenses are Contribute-related on their invoices, prior to submitting to the Company.

----

Return to the main [finance page](/handbook/finance/).

